#!/bin/bash
# Name: auto_update_phpmyadmin.sh
# Description: this script searches for the latest PHPMyAdmin version and updates if necessary.
# Author: Thom Pol
# Written on 22-02-2022
# Tested on systems that were installed according to HowToForge's Perfect Server guides for auto/manual install (Debian 10/11)

# Installation:
# Put this script in /usr/local/sbin/
# curl https://git.ispconfig.org/ispconfig/tools/-/raw/stable/auto_update_phpmyadmin.sh -L -o /usr/local/sbin/auto_update_phpmyadmin.sh
# Make it executable:
# chmod +x /usr/local/sbin/auto_update_phpmyadmin.sh
# Test the script by running it:
# /usr/local/sbin/auto_update_phpmyadmin.sh
# Then create a cronjob:
# 0 4 * * * /usr/local/sbin/auto_update_phpmyadmin.sh
# Eventually set the email variable below to your e-mail address.

# User variables
# E-mail address to send update notifications to when a update has been done. If this variable is empty, no notifications will be send.
email="pma@example.com"
# Directory where phpMyAdmin is installed
phpMyAdmindir="/usr/share/phpmyadmin"

# Static variables
currentversion=$(grep -m 1 "version" /usr/share/phpmyadmin/package.json | grep -oEm 1 "[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}")
latestversion=$(curl -s https://www.phpmyadmin.net/home_page/version.txt | grep -oEm 1 "[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,3}")
latesturl=$(curl -s https://www.phpmyadmin.net/home_page/version.txt | grep -E "https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)")

# Check TempDir permissions
if id -u ispapps >/dev/null 2>&1; then
    chown -R ispapps:www-data /var/lib/phpmyadmin
fi
if [ $(dpkg --list phpmyadmin 2>/dev/null | grep '^i' | wc -l) == "1" ]; then
    echo "phpMyAdmin is installed through apt. Run apt update && apt upgrade to update to the latest available package."
    exit 1
# this should not be necessary as in the new version of this script we can set the phpMyAdmindir ourself and no permissions are touched
#elif [ ! -f /etc/debian_version ]; then
#    echo "This script only supports Debian/Ubuntu systems, but your server seems to be running a different OS."
#    exit 1
elif [ "$currentversion" != "$latestversion" ]; then
    currentdate=$(date +"%y%m%d%H%M%S")
    phpMyAdminbakdir=""$phpMyAdmindir"-bak-"$currentversion"-"$currentdate""
    echo "phpMyAdmin version is out of date, installed version: "$currentversion", latest version: "$latestversion""
    echo "Starting phpMyAdmin update."
    # Move current install to backup directory
    mv "$phpMyAdmindir" "$phpMyAdminbakdir"
    # Download and unpack newest version to the phpMyAdmin directory
    curl "$latesturl" -s -L -o /tmp/phpMyAdmin-"$latestversion"-all-languages.zip
    unzip -q /tmp/phpMyAdmin-"$latestversion"-all-languages.zip -d /tmp
    mv /tmp/phpMyAdmin-"$latestversion"-all-languages "$phpMyAdmindir"
    # Copy old config files to the new installation
    cp "$phpMyAdminbakdir"/config.inc.php "$phpMyAdmindir"/
    if [ -f "$phpMyAdminbakdir"/.htaccess ]; then
        echo ".htaccess file found! Copying to new installation."
        cp "$phpMyAdminbakdir"/.htaccess "$phpMyAdmindir"/
    fi
    # Wait
    sleep 1
    # Announce that we're done
    updatedone="phpMyAdmin has been updated to the latest version ("$latestversion")."
    customconfig="If you had any custom config files other than the config.inc.php and/or a .htaccess file, you have to copy them yourself from "$phpMyAdminbakdir"/ to "$phpMyAdmindir"/"
    if [ "$email" != "pma@example.com" ] && [ "$email" != "" ]; then
        echo "$updatedone" "$customconfig" | mail -s "phpMyAdmin update on $(hostname -f)" "$email"
    fi
    echo "$updatedone"
    echo "$customconfig"
elif [ "$currentversion" == "$latestversion" ]; then
    # Local install is up-to-date
    echo "Local phpMyAdmin install is up-to-date, installed version: "$currentversion", latest version: "$latestversion""
else
    echo "Unknown error. Exiting."
    exit 1
fi
